# BootInfra

Spring-boot based server infrastructure for any server applications (microservice, Web, etc.)

# Features

- Service layer infrastructure based on Spring Boot,
- Suitable for developing microservices, web application etc.
- Extensible command line interface support,
- File based configuration support
- I18N support
