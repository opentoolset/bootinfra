package org.opentoolset.bootinfra;

import org.opentoolset.bootinfra.BIContext.Mode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.support.GenericApplicationContext;

@SpringBootTest
public abstract class AbstractTest {

	static {
		BIContext.mode = Mode.UNIT_TEST;
	}

	@Autowired
	private GenericApplicationContext appContext;

	// ---

	protected GenericApplicationContext getAppContext() {
		return appContext;
	}

	protected void println(String text) {
		System.out.println(text);
	}
}
