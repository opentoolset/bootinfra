package org.opentoolset.bootinfra.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import javax.annotation.PostConstruct;

import org.junit.jupiter.api.Test;
import org.opentoolset.bootinfra.AbstractApplication;
import org.opentoolset.bootinfra.AbstractTest;
import org.opentoolset.bootinfra.i18n.AbstractI18nProvider;
import org.opentoolset.bootinfra.i18n.AbstractI18nProvider.Translatable;
import org.opentoolset.bootinfra.i18n.I18nBundleGenerator;
import org.opentoolset.bootinfra.i18n.LocaleWrapper;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.support.GenericApplicationContext;

@EnableAutoConfiguration
public class MTI18n extends AbstractTest {

	private final Package BASE_PACKAGE = AbstractApplication.class.getPackage();
	private final LocaleWrapper LOCALE_EN = LocaleWrapper.create("en", "US");
	private final LocaleWrapper LOCALE_TR = LocaleWrapper.create("tr", "TR", "ISO-8859-9");
	private List<LocaleWrapper> localeWrappers = Arrays.asList(LOCALE_TR, LOCALE_EN);

	private AbstractI18nProvider i18nProvider;

	// ---

	@Test
	public void testI18NBundleGenerator() {
		I18nBundleGenerator bundleGenerator = new I18nBundleGenerator.Builder(BASE_PACKAGE).setProvidedLocaleWrappers(localeWrappers).build();
		bundleGenerator.generate();
	}

	@Test
	public void tesAbstractI18nProvider() throws Exception {
		{
			String translation = this.i18nProvider.getTranslation(Enum1.CONST1);
			println(translation);
			assertEquals(translation, "CONST1");
		}
		{
			String translation = this.i18nProvider.getTranslation(Enum1.CONST2);
			println(translation);
			assertEquals(translation, "cçgğıioösşuüCÇGĞIİOÖSŞUÜ");
		}
	}

	// ---

	@PostConstruct
	private void postConstruct() {
		GenericApplicationContext appContext = getAppContext();
		appContext.registerBean(AbstractI18nProvider.class, () -> new AbstractI18nProvider() {

			@Override
			public Locale getLocale() {
				return getDefaultLocale();
			}

			@Override
			protected Package getBasePackage() {
				return BASE_PACKAGE;
			}

			@Override
			protected List<LocaleWrapper> getProvidedLocaleWrappers() {
				return MTI18n.this.localeWrappers;
			}

			@Override
			protected Locale getDefaultLocale() {
				return LOCALE_TR.getLocale();
			}
		});

		this.i18nProvider = appContext.getBean(AbstractI18nProvider.class);
	}

	// ---

	enum Enum1 implements Translatable {
		CONST1,
		CONST2,
	}

	enum Enum2 implements Translatable {
		CONST2,
		CONST1,
		CONST3,
	}
}
