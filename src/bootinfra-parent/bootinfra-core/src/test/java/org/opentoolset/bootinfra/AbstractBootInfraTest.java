package org.opentoolset.bootinfra;

import java.nio.file.Paths;

import org.opentoolset.bootinfra.AbstractBootInfraTest.Application;
import org.opentoolset.bootinfra.BIContext.Mode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.GenericApplicationContext;

@SpringBootTest(classes = Application.class)
@EnableAutoConfiguration
public abstract class AbstractBootInfraTest {

	static {
		BIContext.mode = Mode.UNIT_TEST;
	}

	@Autowired
	private GenericApplicationContext appContext;

	// ---

	protected GenericApplicationContext getAppContext() {
		return appContext;
	}

	protected void println(String text) {
		System.out.println(text);
	}

	// ---

	@Configuration
	public static class TestConfiguration extends SpringConfiguration {

		@Override
		protected String getAppHomeFolder() {
			return Paths.get(System.getProperty("user.home")).resolve("bootinfra-test").toString();
		}
	}

	@BootInfraApplication
	public static class Application extends AbstractApplication {
	}
}
