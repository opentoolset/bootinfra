package org.opentoolset.bootinfra.i18n;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Locale;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class LocaleWrapper {

	private Locale locale;
	private Charset charset;

	public static LocaleWrapper create(String language, String country) {
		LocaleWrapper localeWrapper = new LocaleWrapper();
		localeWrapper.locale = new Locale(language, country);
		localeWrapper.charset = StandardCharsets.ISO_8859_1;
		return localeWrapper;
	}

	public static LocaleWrapper create(String language, String country, String charsetName) {
		LocaleWrapper localeWrapper = create(language, country);
		localeWrapper.charset = Charset.forName(charsetName);
		return localeWrapper;
	}

	public Locale getLocale() {
		return locale;
	}

	public Charset getCharset() {
		return charset;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
