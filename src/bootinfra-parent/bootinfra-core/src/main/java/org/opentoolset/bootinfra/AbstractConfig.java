// ---
// Copyright 2020 BootInfra Team
// All rights reserved
// ---
package org.opentoolset.bootinfra;

import java.util.Set;

import org.apache.commons.lang3.ObjectUtils;

public abstract class AbstractConfig {

	public interface Entry {

		public String getKey();

		public Object getDefaultValue();
	}

	// ---

	protected abstract Set<Entry> getEntriesCombined();

	// ---

	public void save() {
		BIContext.configProvider.save();
	}

	// ---

	protected boolean containsKey(String key) {
		for (Entry item : getEntriesCombined()) {
			if (!ObjectUtils.notEqual(item.getKey(), key)) {
				return true;
			}
		}

		return false;
	}

	protected String getString(Entry key) {
		return BIContext.configProvider.getString(key);
	}

	protected boolean getBoolean(Entry key) {
		return BIContext.configProvider.getBoolean(key);
	}

	protected Integer getInteger(Entry key) {
		return BIContext.configProvider.getInteger(key);
	}

	protected void set(Entry key, Object value) {
		BIContext.configProvider.set(key, value);
	}
}
