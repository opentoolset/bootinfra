package org.opentoolset.bootinfra;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import ch.qos.logback.classic.Level;

public class BIConfig extends AbstractConfig {

	private enum Entry implements AbstractConfig.Entry {

		LOGGING_LEVEL("logging.level", BIConstants.DEFAULT_LOGGING_LEVEL.levelStr);

		private String key;

		private Object defaultValue;

		private <T> Entry(String key, Object defaultValue) {
			this.key = key;
			this.defaultValue = defaultValue;
		}

		public String getKey() {
			return key;
		}

		public Object getDefaultValue() {
			return defaultValue;
		}
	}

	// ---

	public Level getLoggingLevel() {
		String logginLevelString = getString(Entry.LOGGING_LEVEL);
		return Level.toLevel(logginLevelString, BIConstants.DEFAULT_LOGGING_LEVEL);
	}

	public void setLoggingLevel(Level level) {
		set(Entry.LOGGING_LEVEL, level.levelStr);
	}

	// ---

	@Override
	protected Set<AbstractConfig.Entry> getEntriesCombined() {
		return new HashSet<>(Arrays.asList(Entry.values()));
	}
}
