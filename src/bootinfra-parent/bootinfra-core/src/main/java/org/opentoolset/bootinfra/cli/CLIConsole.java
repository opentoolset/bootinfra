// ---
// Copyright 2020 BootInfra Team
// All rights reserved
// ---
package org.opentoolset.bootinfra.cli;

import java.io.IOException;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.jline.utils.AttributedString;
import org.springframework.shell.jline.PromptProvider;

import jline.Terminal;
import jline.console.ConsoleReader;

public class CLIConsole {

	private ConsoleReader consoleReader;

	private final AttributedString DEFAULT_SHELL_PROMPT = new AttributedString("bootinfra:> ");

	private final String DEFAULT_SELECTION_PROMPT = "Select one option (0 to exit)";

	// ---

	public PromptProvider getShellPromptProvider() {
		return () -> DEFAULT_SHELL_PROMPT;
	}

	public String getSelectionPrompt() {
		return DEFAULT_SELECTION_PROMPT;
	}

	public String getInput(String prompt, Predicate<String> validator) throws IOException {
		String input;
		while (true) {
			input = this.consoleReader.readLine(prompt);
			try {
				if (validator.test(input)) {
					break;
				}
			} catch (Exception e) {
			}
		}

		return input;
	}

	public <T> T select(List<Selectable<T>> selectables, String mainPrompt) throws IOException {
		String listStr = IntStream.range(1, selectables.size() + 1).boxed().map(index -> String.format("%s - %s", index, selectables.get(index - 1).title)).collect(Collectors.joining("\n"));
		println(mainPrompt);
		println(listStr);

		while (true) {
			String selectionStr = this.consoleReader.readLine(String.format("%s: ", getSelectionPrompt()));
			try {
				int selectionIndex = Integer.parseInt(selectionStr);
				if (selectionIndex == 0) {
					return null;
				}

				return selectables.get(selectionIndex - 1).item;
			} catch (Exception e) {
			}
		}
	}

	public void println(String format, Object... args) {
		print(format + "\n", args);
	}

	public void print(String format, Object... args) {
		System.out.printf(format, args);
	}

	// ---

	@PostConstruct
	private void postConstruct() throws IOException {
		println("CLI is starting...");
		this.consoleReader = new ConsoleReader();
	}

	@PreDestroy
	private void preDestroy() throws Exception {
		println("CLI is closing...");
		Terminal terminal = this.consoleReader.getTerminal();
		if (terminal != null) {
			terminal.reset();
		}
	}

	// ---

	public static class Selectable<T> {

		private T item;

		private String title;

		public Selectable(T item, String title) {
			this.item = item;
			this.title = title;
		}
	}
}