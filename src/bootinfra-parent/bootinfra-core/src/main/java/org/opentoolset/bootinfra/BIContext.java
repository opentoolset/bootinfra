// ---
// Copyright 2020 BootInfra Team
// All rights reserved
// ---
package org.opentoolset.bootinfra;

public class BIContext {

	public enum Mode {
		LIVE,
		POPULATOR,
		UNIT_TEST,
	}

	public static Mode mode = Mode.LIVE;

	public static BIConfigProvider configProvider;
}
