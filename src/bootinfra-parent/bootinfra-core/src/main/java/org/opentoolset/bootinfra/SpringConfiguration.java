// ---
// Copyright 2020 BootInfra Team
// All rights reserved
// ---
package org.opentoolset.bootinfra;

import org.opentoolset.bootinfra.cli.CLIConsole;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.shell.jline.PromptProvider;

@Configuration
public abstract class SpringConfiguration {

	protected abstract String getAppHomeFolder();

	@Bean
	protected BIConfig config() throws Exception {
		BIConfig config = buildConfig();
		BIContext.configProvider = new BIConfigProvider(getAppHomeFolder(), config);
		BILogger.configure(getAppHomeFolder(), config);
		return config;
	}

	@Bean
	protected PromptProvider promptProvider() {
		return cliConsole().getShellPromptProvider();
	}

	@Bean
	protected CLIConsole cliConsole() {
		return new CLIConsole();
	}

	protected BIConfig buildConfig() {
		return new BIConfig();
	}
}
