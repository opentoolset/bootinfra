// ---
// Copyright 2020 BootInfra Team
// All rights reserved
// ---
package org.opentoolset.bootinfra;

import java.nio.file.Path;
import java.nio.file.Paths;

import ch.qos.logback.classic.Level;

public interface BIConstants {

	String CONFIG_FOLDER = "etc";
	String CONFIG_FILE = "app.conf";
	String LOG_FOLDER = "log";
	String LOG_FILE = "log.txt";
	String LOG_FILE_PATTERN_FOR_ROLLING = "log-%d{yyyyMMdd}.txt";
	Level DEFAULT_LOGGING_LEVEL = Level.INFO;
	Path DEFAULT_HOME_FOLDER = Paths.get(System.getProperty("user.home")).resolve("bootinfra-demo");
}
