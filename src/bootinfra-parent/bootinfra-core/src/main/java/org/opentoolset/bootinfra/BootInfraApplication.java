package org.opentoolset.bootinfra;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.boot.autoconfigure.SpringBootApplication;

@Retention(value = RetentionPolicy.RUNTIME)
@Target(value = ElementType.TYPE)
@SpringBootApplication
public @interface BootInfraApplication {
}