package org.opentoolset.bootinfra.i18n;

public class I18nConverter {

	private Package basePackage;

	public I18nConverter(Package basePackage) {
		this.basePackage = basePackage;
	}

	public String convertEnumConstantToKey(Enum<?> enumConstant) {
		String keyBase = convertEnumToKey(enumConstant.getClass());
		return concat(keyBase, enumConstant.name());
	}

	public String convertEnumToKey(Class<?> enumType) {
		return enumType.getCanonicalName().replaceFirst(this.basePackage.getName() + ".", "");
	}

	public String concat(String str1, String str2) {
		return String.format("%s.%s", str1, str2);
	}
}
