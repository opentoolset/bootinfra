package org.opentoolset.bootinfra.i18n;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.stream.Collectors;

import org.opentoolset.bootinfra.BIContext;
import org.opentoolset.bootinfra.BIContext.Mode;
import org.opentoolset.bootinfra.i18n.AbstractI18nProvider.Translatable;
import org.reflections.Reflections;

public class I18nBundleGenerator {

	private String resourceBundleFolder = AbstractI18nProvider.DEFAULT_RESOURCE_BUNDLE_FOLDER;
	private String resourceBundleFilePrefix = AbstractI18nProvider.DEFAULT_RESOURCE_BUNDLE_FILE_PREFIX;
	private List<LocaleWrapper> localeWrappers;

	private I18nConverter converter;
	private Reflections reflections;

	private I18nBundleGenerator(Package basePackage) {
		this.converter = new I18nConverter(basePackage);
		this.reflections = new Reflections(basePackage.getName());
	}

	public void generate() {
		for (LocaleWrapper locale : localeWrappers) {
			generate(locale);
		}
	}

	// ---

	private void generate(LocaleWrapper localeWrapper) {
		Locale locale = localeWrapper.getLocale();
		Charset charset = localeWrapper.getCharset();

		String lang = locale.getLanguage();
		String country = locale.getCountry();
		String fileName = String.format("%s_%s_%s.properties", resourceBundleFilePrefix, lang, country);

		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		String pathStr = loader.getResource("").getPath();
		pathStr = pathStr.replaceFirst("target/test-classes/$", "");

		Path basePath = Paths.get(pathStr);
		if (BIContext.mode == Mode.UNIT_TEST) {
			basePath = basePath.resolve("src/test/resources");
		} else {
			basePath = basePath.resolve("src/main/resources");
		}

		basePath = basePath.resolve(this.resourceBundleFolder);

		try {
			Files.createDirectories(basePath);
			Path filePath = basePath.resolve(fileName);

			Map<String, String> existingProperties = buildBundlePropertiesWithExisting(filePath, charset);

			SortedMap<String, String> newProperties = new TreeMap<>();
			populateNewProperties(newProperties, existingProperties);
			writeNewProperties(newProperties, filePath, charset);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private Map<String, String> buildBundlePropertiesWithExisting(Path existingBundle, Charset charset) throws IOException {
		Map<String, String> existingProperties = new HashMap<>();

		try {
			List<String> lines = Files.readAllLines(existingBundle, charset);
			for (String line : lines) {
				int index = line.indexOf("=");
				if (index > -1) {
					String key = line.substring(0, index).trim();
					if (!key.isEmpty()) {
						existingProperties.put(key, line.substring(index + 1).trim());
					}
				}
			}
		} catch (NoSuchFileException e) {
			Files.createFile(existingBundle);
		}

		return existingProperties;
	}

	private void populateNewProperties(SortedMap<String, String> newProperties, Map<String, String> existingProperties) {
		Set<Class<? extends Translatable>> types = this.reflections.getSubTypesOf(Translatable.class);
		for (Class<? extends Translatable> type : types) {
			if (type.isEnum()) {
				populateNewProperties(newProperties, type, existingProperties);
			}
		}
	}

	private void populateNewProperties(SortedMap<String, String> newProperties, Class<? extends Translatable> enumType, Map<String, String> existingProperties) {
		String keyBase = this.converter.convertEnumToKey(enumType);
		for (Object enumConst : enumType.getEnumConstants()) {
			Enum<?> translatableEnum = (Enum<?>) enumConst;
			String value = translatableEnum.name();
			String key = this.converter.concat(keyBase, value);
			String existingValue = existingProperties.get(key);
			newProperties.put(key, existingValue != null ? existingValue : value);
		}
	}

	private void writeNewProperties(SortedMap<String, String> newProperties, Path filePath, Charset charset) throws IOException {
		List<String> lines = newProperties.entrySet().stream().map(entry -> String.format("%s = %s", entry.getKey(), entry.getValue())).collect(Collectors.toList());
		Files.write(filePath, lines, charset, StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
	}

	// ---

	public static class Builder {

		private I18nBundleGenerator built;

		public Builder(Package basePackage) {
			this.built = new I18nBundleGenerator(basePackage);
		}

		public Builder setProvidedLocaleWrappers(List<LocaleWrapper> localeWrappers) {
			this.built.localeWrappers = localeWrappers;
			return this;
		}

		public Builder setResourceBundleFolder(String resourceBundleFolder) {
			this.built.resourceBundleFolder = resourceBundleFolder;
			return this;
		}

		public Builder setResourceBundleFilePrefix(String resourceBundleFilePrefix) {
			this.built.resourceBundleFilePrefix = resourceBundleFilePrefix;
			return this;
		}

		public I18nBundleGenerator build() {
			return this.built;
		}
	}
}
