package org.opentoolset.bootinfra.i18n;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.ResourceBundle.Control;

import javax.annotation.PostConstruct;

import org.slf4j.LoggerFactory;

public abstract class AbstractI18nProvider {

	public static final String DEFAULT_RESOURCE_BUNDLE_FOLDER = "i18n";
	public static final String DEFAULT_RESOURCE_BUNDLE_FILE_PREFIX = "translate";

	private List<Locale> locales = new ArrayList<>();
	private Map<Locale, ResourceBundle> bundles = new HashMap<>();
	private I18nConverter converter;

	// ---

	public abstract Locale getLocale();

	protected abstract Package getBasePackage();

	protected abstract List<LocaleWrapper> getProvidedLocaleWrappers();

	protected abstract Locale getDefaultLocale();

	// ---

	public String getTranslation(Translatable translatable, Object... params) {
		if (translatable.getClass().isEnum()) {
			Enum<?> enumConstant = (Enum<?>) translatable;
			String key = converter.convertEnumConstantToKey(enumConstant);
			return getTranslation(key, params);
		}

		LoggerFactory.getLogger(AbstractI18nProvider.class.getName()).warn("Translatable item cannot be resolved to a bundle property");
		return null;
	}

	public String getTranslation(String key, Object... params) {
		Locale locale = getLocale();

		if (locale == null) {
			LoggerFactory.getLogger(AbstractI18nProvider.class.getName()).warn("No locale found");
			return "!" + key;
		}

		return getTranslation(key, locale, params);
	}

	public String getTranslation(String key, Locale locale, Object... params) {
		if (key == null) {
			LoggerFactory.getLogger(AbstractI18nProvider.class.getName()).warn("Got lang request for key with null value!");
			return "";
		}

		ResourceBundle bundle = bundles.get(locale);
		if (bundle == null) {
			LoggerFactory.getLogger(AbstractI18nProvider.class.getName()).warn("Locale is not supported! Locale: {}", locale);
			return "!" + locale.getLanguage() + ": " + key;
		}

		String value;
		try {
			value = bundle.getString(key);
		} catch (MissingResourceException e) {
			LoggerFactory.getLogger(AbstractI18nProvider.class.getName()).warn("Missing resource", e);
			return "!" + locale.getLanguage() + ": " + key;
		}

		if (params.length > 0) {
			value = MessageFormat.format(value, params);
		}

		return value;
	}

	public ResourceBundle loadBundle(String resourceFolder, String bundleFilePrefix, LocaleWrapper localeWrapper) {
		Path path = Paths.get(resourceFolder).resolve(bundleFilePrefix);

		try {
			if (localeWrapper.getCharset() != null) {
				return ResourceBundle.getBundle(path.toString(), localeWrapper.getLocale(), new CharsetControl(localeWrapper.getCharset().name()));
			} else {
				return ResourceBundle.getBundle(path.toString(), localeWrapper.getLocale());
			}
		} catch (final MissingResourceException e) {
			LoggerFactory.getLogger(AbstractI18nProvider.class.getName()).warn("Missing resource", e);
			return null;
		}
	}

	// ---

	@PostConstruct
	protected void postConstruct() {
		for (LocaleWrapper localeWrapper : getProvidedLocaleWrappers()) {
			this.locales.add(localeWrapper.getLocale());
			ResourceBundle bundle = loadBundle(getResourceBundleFolder(), getResourceBundleFilePrefix(), localeWrapper);
			this.bundles.put(localeWrapper.getLocale(), bundle);
		}

		this.locales = Collections.unmodifiableList(this.locales);
		this.converter = new I18nConverter(getBasePackage());
	}

	protected String getResourceBundleFolder() {
		return DEFAULT_RESOURCE_BUNDLE_FOLDER;
	}

	protected String getResourceBundleFilePrefix() {
		return DEFAULT_RESOURCE_BUNDLE_FILE_PREFIX;
	}

	protected List<Locale> getLocales() {
		return this.locales;
	}

	// ---

	public interface Translatable {
	}

	// ---

	private static final class CharsetControl extends Control {

		private final String charset;

		public CharsetControl(String charset) {
			this.charset = charset;
		}

		@Override
		public ResourceBundle newBundle(String baseName, Locale locale, String format, ClassLoader loader, boolean reload) throws IllegalAccessException, InstantiationException, IOException {
			String bundleName = toBundleName(baseName, locale);
			String resourceName = toResourceName(bundleName, "properties");
			return "java.properties".equals(format) ? newPropertyResourceBundle(resourceName, loader) : super.newBundle(baseName, locale, format, loader, reload);
		}

		private ResourceBundle newPropertyResourceBundle(String resourceName, ClassLoader loader) throws IOException {
			try (InputStream stream = loader.getResourceAsStream(resourceName); InputStreamReader reader = new InputStreamReader(stream, charset)) {
				return new PropertyResourceBundle(reader);
			}
		}
	}
}
