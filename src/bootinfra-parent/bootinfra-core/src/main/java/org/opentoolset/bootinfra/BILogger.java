// ---
// Copyright 2020 BootInfra Team
// All rights reserved
// ---
package org.opentoolset.bootinfra;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.helpers.MessageFormatter;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.rolling.RollingFileAppender;
import ch.qos.logback.core.rolling.TimeBasedRollingPolicy;
import ch.qos.logback.core.util.FileSize;

public class BILogger {

	private static LoggerContext loggerContext;
	private static ch.qos.logback.classic.Logger logger;

	static {
		Logger genericLogger = LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
		if (genericLogger instanceof ch.qos.logback.classic.Logger) {
			logger = (ch.qos.logback.classic.Logger) genericLogger;
			loggerContext = logger.getLoggerContext();
		} else {
			throw new IllegalStateException(String.format("ch.qos.logback.classic.Logger is required. Current one is: %s", logger));
		}
	}

	public static void configure(String homeFolder, BIConfig config) throws Exception {
		try {
			Path homeFolderPath = Paths.get(homeFolder);

			Path logFolderPath = homeFolderPath.resolve(BIConstants.LOG_FOLDER);
			BILogger.info("Log directory is {}", logFolderPath);
			Files.createDirectories(logFolderPath);

			Path logFilePath = logFolderPath.resolve(BIConstants.LOG_FILE);
			BILogger.info("Log file: {}", logFilePath);
			if (!Files.exists(logFilePath)) {
				Files.createFile(logFilePath);
				BILogger.info("Log file was created {}", logFilePath);
			}

			loggerContext.reset();

			RollingFileAppender<ILoggingEvent> appender = new RollingFileAppender<>();
			{
				appender.setContext(loggerContext);
				appender.setFile(logFilePath.toString());

				PatternLayoutEncoder encoder = new PatternLayoutEncoder();
				{
					encoder.setContext(loggerContext);
					encoder.setPattern("%date %level [%thread] %logger{10} [%file:%line] %msg%n");
					encoder.start();
					appender.setEncoder(encoder);
				}

				TimeBasedRollingPolicy<ILoggingEvent> policy = new TimeBasedRollingPolicy<>();
				{
					policy.setContext(loggerContext);
					policy.setFileNamePattern(logFolderPath.resolve(BIConstants.LOG_FILE_PATTERN_FOR_ROLLING).toString());
					policy.setMaxHistory(30);
					policy.setTotalSizeCap(FileSize.valueOf("1024MB"));
					policy.setParent(appender);
					policy.start();
					appender.setRollingPolicy(policy);
				}

				appender.start();
				logger.addAppender(appender);
			}

			logger.setLevel(config.getLoggingLevel());
			logger.setAdditive(false);

			BILogger.info("test..................");
		} catch (Exception e) {
			BILogger.warn(e);
			throw e;
		}
	}

	public static Logger getLogger() {
		return logger;
	}

	public static void debug(String format, Object... args) {
		logger.debug(format, args);
	}

	public static void info(String format, Object... args) {
		logger.info(format, args);
	}

	public static void warn(String format, Object... args) {
		logger.warn(format, args);
	}

	public static void error(String format, Object... args) {
		logger.error(format, args);
	}

	public static void debug(Throwable e, String format, Object... args) {
		String msg = MessageFormatter.format(format, args).getMessage();
		logger.debug(msg, e);
	}

	public static void warn(Throwable e, String format, Object... args) {
		String msg = MessageFormatter.format(format, args).getMessage();
		logger.warn(msg, e);
	}

	public static void error(Throwable e, String format, Object... args) {
		String msg = MessageFormatter.format(format, args).getMessage();
		logger.error(msg, e);
	}

	public static void warn(Throwable e) {
		logger.warn("", e);
	}

	public static void error(Throwable e) {
		logger.error("", e);
	}

	public static void entering() {
		StackTraceElement element = getCallingMethod();
		logger.info("entering {}.{}", element.getClassName(), element.getMethodName());
	}

	public static void exiting() {
		StackTraceElement element = getCallingMethod();
		logger.info("exiting {}.{}", element.getClassName(), element.getMethodName());
	}

	// ---

	private static StackTraceElement getCallingMethod() {
		StackTraceElement stackTraceElement = Thread.currentThread().getStackTrace()[3];
		return stackTraceElement;
	}
}
