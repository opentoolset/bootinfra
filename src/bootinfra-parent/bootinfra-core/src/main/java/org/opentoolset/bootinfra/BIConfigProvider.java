// ---
// Copyright 2020 BootInfra Team
// All rights reserved
// ---
package org.opentoolset.bootinfra;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.apache.commons.configuration2.FileBasedConfiguration;
import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.builder.ConfigurationBuilderEvent;
import org.apache.commons.configuration2.builder.ReloadingFileBasedConfigurationBuilder;
import org.apache.commons.configuration2.builder.fluent.FileBasedBuilderParameters;
import org.apache.commons.configuration2.builder.fluent.Parameters;
import org.apache.commons.configuration2.event.ConfigurationEvent;
import org.apache.commons.configuration2.event.EventListener;
import org.apache.commons.configuration2.event.EventType;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.commons.configuration2.reloading.PeriodicReloadingTrigger;
import org.apache.commons.configuration2.reloading.ReloadingController;
import org.apache.commons.configuration2.reloading.ReloadingEvent;
import org.opentoolset.bootinfra.AbstractConfig.Entry;

import com.google.common.collect.Lists;

public class BIConfigProvider {

	private ReloadingFileBasedConfigurationBuilder<FileBasedConfiguration> configBuilder;
	private Path configFilePath;
	private BIConfig config;

	public BIConfigProvider(String homeFolder, BIConfig config) throws Exception {
		this.config = config;
		try {
			Path homeFolderPath = Paths.get(homeFolder);
			BILogger.info("Home folder: {}", homeFolderPath);

			Path configFolderPath = homeFolderPath.resolve(BIConstants.CONFIG_FOLDER);
			BILogger.info("Config directory is {}", configFolderPath);
			Files.createDirectories(configFolderPath);

			configFilePath = configFolderPath.resolve(BIConstants.CONFIG_FILE);
			BILogger.info("Config file: {}", configFilePath);
			if (!Files.exists(configFilePath)) {
				Files.createFile(configFilePath);
				BILogger.info("Config file was created {}", configFilePath);
			}
		} catch (Exception e) {
			BILogger.warn(e);
			throw e;
		}

		loadConfig();
	}

	// ---

	public BIConfig getConfig() {
		return config;
	}

	public String getString(Entry key) {
		try {
			return getFileBasedConfig().getString(key.getKey(), key.getDefaultValue().toString());
		} catch (Exception e) {
			return null;
		}
	}

	public Integer getInteger(Entry key) {
		try {
			return getFileBasedConfig().getInteger(key.getKey(), (Integer) key.getDefaultValue());
		} catch (Exception e) {
			return null;
		}
	}

	public boolean getBoolean(Entry key) {
		try {
			return getFileBasedConfig().getBoolean(key.getKey(), (boolean) key.getDefaultValue());
		} catch (Exception e) {
			return false;
		}
	}

	public void setAndSave(Entry key, Object value) {
		set(key, value);
		save();
	}

	public void set(Entry key, Object value) {
		getFileBasedConfig().setProperty(key.getKey(), Optional.ofNullable(value).orElse(""));
	}

	public void save() {
		try {
			configBuilder.save();
		} catch (ConfigurationException e) {
			BILogger.error(e);
		}
	}

	public <T extends ConfigurationEvent> void addConfigurationEventListener(EventType<T> eventType, EventListener<? super T> listener) {
		configBuilder.addEventListener(eventType, listener);
	}

	public <T extends ReloadingEvent> void addReloadingEventListener(EventType<T> eventType, EventListener<? super T> listener) {
		configBuilder.getReloadingController().addEventListener(eventType, listener);
	}

	// ---

	private void loadConfig() {
		try {
			boolean changed = false;
			FileBasedConfiguration fileBasedConfig = getFileBasedConfig();
			List<Entry> entries = this.config.getEntriesCombined().stream().sorted((o1, o2) -> o1.getKey().compareTo(o2.getKey())).collect(Collectors.toList());
			for (AbstractConfig.Entry item : entries) {
				String key = item.getKey();
				if (!fileBasedConfig.containsKey(key)) {
					fileBasedConfig.setProperty(key, Optional.ofNullable(item.getDefaultValue()).orElse(""));
					changed = true;
				}
			}

			for (String key : Lists.newArrayList(fileBasedConfig.getKeys())) {
				if (!this.config.containsKey(key)) {
					fileBasedConfig.clearProperty(key);
					changed = true;
				}
			}

			if (changed) {
				configBuilder.save();
				BILogger.info("Config file was completed with defaults");
			}
		} catch (ConfigurationException e) {
			BILogger.error(e);
		}
	}

	private FileBasedConfiguration getFileBasedConfig() {
		if (configBuilder == null) {
			configBuilder = createConfigBuilder();
		}

		try {
			return configBuilder.getConfiguration();
		} catch (ConfigurationException e) {
			BILogger.error(e);
			System.exit(-1);
			return null;
		}
	}

	private ReloadingFileBasedConfigurationBuilder<FileBasedConfiguration> createConfigBuilder() {
		ReloadingFileBasedConfigurationBuilder<FileBasedConfiguration> configBuilder = new ReloadingFileBasedConfigurationBuilder<FileBasedConfiguration>(PropertiesConfiguration.class);
		{
			File configFile = configFilePath.toFile();
			FileBasedBuilderParameters params = new Parameters().fileBased().setFile(configFile);
			configBuilder.configure(params);
			BILogger.info("Config file was loaded {}", configFile);
		}

		configBuilder.addEventListener(ConfigurationBuilderEvent.RESET, event -> configBuilder.getReloadingController().resetReloadingState());

		ReloadingController reloadingController = configBuilder.getReloadingController();
		PeriodicReloadingTrigger trigger = new PeriodicReloadingTrigger(reloadingController, null, 10, TimeUnit.SECONDS);
		trigger.start();

		return configBuilder;
	}
}
