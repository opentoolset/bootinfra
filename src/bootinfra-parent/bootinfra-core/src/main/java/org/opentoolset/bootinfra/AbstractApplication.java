// ---
// Copyright 2020 BootInfra Team
// All rights reserved
// ---
package org.opentoolset.bootinfra;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.ArrayUtils;
import org.opentoolset.bootinfra.BIContext.Mode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.shell.jline.InteractiveShellApplicationRunner;
import org.springframework.util.StringUtils;

public abstract class AbstractApplication {

	@Autowired
	private ConfigurableEnvironment environment;

	// ---

	@PostConstruct
	protected void postConstruct() throws Exception {
		if (BIContext.mode != Mode.LIVE) {
			InteractiveShellApplicationRunner.disable(environment);
		}
	}

	// ---

	public static void run(String[] args, Class<?>... appClasses) {
		SpringApplicationBuilder appBuilder = new SpringApplicationBuilder(appClasses);
		{
			List<String> disabledCommands = new ArrayList<>();
			disabledCommands.add("--spring.shell.command.quit.enabled=false");
			String[] fullArgs = StringUtils.concatenateStringArrays(args, ArrayUtils.toStringArray(disabledCommands.toArray(), ""));

			appBuilder.web(WebApplicationType.NONE);
			appBuilder.logStartupInfo(false);
			appBuilder.run(fullArgs);
		}
	}
}
