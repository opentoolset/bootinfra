package org.opentoolset.bootinfra.demo;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import org.opentoolset.bootinfra.i18n.AbstractI18nProvider;
import org.opentoolset.bootinfra.i18n.LocaleWrapper;
import org.springframework.stereotype.Component;

@Component
public class DemoTranslator extends AbstractI18nProvider {

	public static final Package BASE_PACKAGE = DemoApplication.class.getPackage();
	private static final LocaleWrapper LOCALE_EN = LocaleWrapper.create("en", "US");
	private static final LocaleWrapper LOCALE_TR = LocaleWrapper.create("tr", "TR", "ISO-8859-9");
	public static final List<LocaleWrapper> PROVIDED_LOCALE_WRAPPERS = Arrays.asList(LOCALE_EN, LOCALE_TR);
	private Locale activeLocale = getDefaultLocale();

	@Override
	public Locale getLocale() {
		return this.activeLocale;
	}

	@Override
	protected Package getBasePackage() {
		return BASE_PACKAGE;
	}

	@Override
	protected List<LocaleWrapper> getProvidedLocaleWrappers() {
		return PROVIDED_LOCALE_WRAPPERS;
	}

	@Override
	protected Locale getDefaultLocale() {
		return LOCALE_EN.getLocale();
	}

	public void setAciveLocale(Locale activeLocale) {
		this.activeLocale = activeLocale;
	}
}
