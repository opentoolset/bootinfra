// ---
// Copyright 2020 BootInfra Team
// All rights reserved
// ---
package org.opentoolset.bootinfra.demo;

import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import org.opentoolset.bootinfra.cli.CLIConsole;
import org.opentoolset.bootinfra.i18n.AbstractI18nProvider.Translatable;
import org.opentoolset.bootinfra.i18n.LocaleWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;

@ShellComponent(value = "Demo Shell")
public class DemoShell {

	@Autowired
	private CLIConsole console;

	@Autowired
	private DemoTranslator translator;

	// ---

	@ShellMethod("Switch language")
	public void switchLang() throws Exception {
		console.println("%s: %s", getTranslation(I18n.ACTIVE_LANGUAGE), this.translator.getLocale());
		List<LocaleWrapper> providedLocaleWrappers = this.translator.getProvidedLocaleWrappers();
		LocaleWrapper selection = console.select(providedLocaleWrappers.stream().map(lw -> new CLIConsole.Selectable<LocaleWrapper>(lw, lw.toString())).collect(Collectors.toList()), this.translator.getTranslation(I18n.SELECT_LANGUAGE));
		if (selection != null) {
			Locale selectedLocale = selection.getLocale();
			this.translator.setAciveLocale(selectedLocale);
			console.println(getTranslation(I18n.SWICH_LANG_COMMAND_RESULT, selectedLocale));
		}
	}

	@ShellMethod("Test")
	public void test() throws Exception {
		console.println(getTranslation(I18n.TEST_COMMAND_RESULT));
	}

	// ---

	private String getTranslation(I18n testCommandResult, Object... params) {
		return this.translator.getTranslation(testCommandResult, params);
	}

	// ---

	private enum I18n implements Translatable {
		ACTIVE_LANGUAGE,
		SELECT_LANGUAGE,
		SWICH_LANG_COMMAND_RESULT,
		TEST_COMMAND_RESULT,
	}
}