// ---
// Copyright 2020 BootInfra Team
// All rights reserved
// ---
package org.opentoolset.bootinfra.demo;

import org.opentoolset.bootinfra.AbstractApplication;
import org.opentoolset.bootinfra.BootInfraApplication;

@BootInfraApplication
public class DemoApplication extends AbstractApplication {

	public static void main(String[] args) {
		AbstractApplication.run(args, DemoApplication.class);
	}
}
