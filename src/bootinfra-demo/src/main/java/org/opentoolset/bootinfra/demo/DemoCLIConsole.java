// ---
// Copyright 2020 BootInfra Team
// All rights reserved
// ---
package org.opentoolset.bootinfra.demo;

import org.jline.utils.AttributedString;
import org.opentoolset.bootinfra.cli.CLIConsole;
import org.springframework.shell.jline.PromptProvider;

public class DemoCLIConsole extends CLIConsole {

	private final AttributedString SHELL_PROMPT = new AttributedString("bootinfra-demo:> ");

	@Override
	public PromptProvider getShellPromptProvider() {
		return () -> SHELL_PROMPT;
	}
}
